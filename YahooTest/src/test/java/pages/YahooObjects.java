package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class YahooObjects {

	public static void main(String[] args) throws InterruptedException {
		// Creating webdriver obj
		WebDriver driver = null;
		// setting system path
		System.setProperty("webdriver.chrome.driver", ".\\libs\\chromedriver.exe");
		// creating new chromedriver obj
		driver = new ChromeDriver();
		// invoking the browser
		driver.get("http://www.yahoo.com");
		//max screen
		driver.manage().window().maximize();
		// sending keys to element
		WebElement textBoxSearch = driver.findElement(By.xpath("//input[@id='header-search-input']"));
		// sending keys to textbox search box
		textBoxSearch.sendKeys("Weather in irving");
		// creating Webelement object for search button
		WebElement searchButton = driver.findElement(By.xpath("//button[contains(@id,'header-desktop')]"));
		// clicking on the search Button object
		searchButton.click();
		// close the browser
		
	}
}
